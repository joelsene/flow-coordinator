//
//  LoginViewController.swift
//  Tabbar Coordinator
//
//  Created by BRQ on 28/11/18.
//  Copyright © 2018 BRQ. All rights reserved.
//

import UIKit

protocol LoginViewControllerDelegate: class {
    func loginViewController(_ libraryViewController: LoginViewController)
}

class LoginViewController: UIViewController, StoryboardInstantiable {
    static let storyboardName = "LoginViewController"
    
    var delegate: LoginViewControllerDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()

    }
    

    @IBAction func loginAction(_ sender: Any) {
        print(self)

        self.delegate?.loginViewController(self)
    }
    
}
