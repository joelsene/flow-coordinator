//
//  ProfileViewController.swift
//  Tabbar Coordinator
//
//  Created by BRQ on 29/11/18.
//  Copyright © 2018 BRQ. All rights reserved.
//

import UIKit

protocol ProfileViewControllerDelegate: class {
    func artistsViewController(_ artistsViewController: ProfileViewController)
}

class ProfileViewController: UIViewController, StoryboardInstantiable {
    static var storyboardName: String = "ProfileViewController"
    
    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var labelText: UILabel!
    var receivedText: String?
    
    var delegate: ProfileViewControllerDelegate?
    override func viewDidLoad() {
        super.viewDidLoad()
        if receivedText == "Push" {
            labelText.text = "ProfileViewController sendo apresentada com um \(String(describing: receivedText!))"
            closeButton.isHidden = true
            self.view.backgroundColor = #colorLiteral(red: 0.9607843161, green: 0.7058823705, blue: 0.200000003, alpha: 1)
        } else {
            labelText.text = "ProfileViewController sendo apresentada com um \(String(describing: receivedText!))"
            self.view.backgroundColor = #colorLiteral(red: 0.1764705926, green: 0.4980392158, blue: 0.7568627596, alpha: 1)
        }
        
        
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func closeAction(_ sender: Any) {
        self.delegate?.artistsViewController(self)
    }
    
}
