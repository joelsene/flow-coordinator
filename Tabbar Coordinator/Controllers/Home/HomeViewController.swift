//
//  HomeViewController.swift
//  Tabbar Coordinator
//
//  Created by BRQ on 29/11/18.
//  Copyright © 2018 BRQ. All rights reserved.
//

import UIKit

protocol HomeViewControllerDelegate: class {
    func pushViewController(_ pushViewController: HomeViewController, text: String)
    func presentViewController(_ presentViewController: HomeViewController, text: String)
}

class HomeViewController: UIViewController, StoryboardInstantiable {
    static var storyboardName = "HomeViewController"
    var delegate: HomeViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
    }
    @IBAction func presentAction(_ sender: Any) {
        self.delegate?.presentViewController(self, text: "Present")
    }
    
    @IBAction func pushAction(_ sender: Any) {
        self.delegate?.pushViewController(self, text: "Push")
    }
    
}


