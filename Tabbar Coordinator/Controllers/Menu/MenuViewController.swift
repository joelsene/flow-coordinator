//
//  MenuViewController.swift
//  Tabbar Coordinator
//
//  Created by BRQ on 29/11/18.
//  Copyright © 2018 BRQ. All rights reserved.
//

import UIKit

protocol MenuViewControllerDelegate: class {
    func menuViewController(_ menuViewController: MenuViewController)
}

class MenuViewController: UIViewController, StoryboardInstantiable {
    static var storyboardName: String = "MenuViewController"
    weak var delegate: MenuViewControllerDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        title = "Perfil"
    }
    
    @IBAction func logoutAction(_ sender: Any) {
        delegate?.menuViewController(self)
    }
    
}
