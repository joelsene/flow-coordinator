//
//  TabbarCoordinator.swift
//  Tabbar Coordinator
//
//  Created by BRQ on 29/11/18.
//  Copyright © 2018 BRQ. All rights reserved.
//

import UIKit

class TabbarCoordinator: BaseCoordinator {
    
    typealias Dependencies = HasArtistFetcher & HasSongFetcher
    
    init(dependencies: Dependencies) {
        let homeCoordinator = HomeCoordinator()
        let menuCoordinator = MenuCoordinator()
        let tabBarController = TabbarViewController()
        
        
        
        homeCoordinator.rootViewController.tabBarItem = UITabBarItem(title: "Home", image: UIImage(named: "iconeHome"), tag: 0)
        menuCoordinator.rootViewController.tabBarItem = UITabBarItem(title: "Menu", image: UIImage(named: "Icn_Menu_More_Selected"), tag: 1)
        
        
        let tabs = [homeCoordinator.rootViewController, menuCoordinator.rootViewController]
        tabBarController.viewControllers = tabs
        
        super.init(rootViewController: tabBarController)
        addChildCoordinator(homeCoordinator)
        addChildCoordinator(menuCoordinator)
    }
    
}
