//
//  AppCoordinator.swift
//  Tabbar Coordinator
//
//  Created by BRQ on 28/11/18.
//  Copyright © 2018 BRQ. All rights reserved.
//

import UIKit

class AppCoordinator: BaseCoordinator {
    
    typealias Dependencies = HasArtistFetcher & HasSongFetcher
    
    init(dependencies: Dependencies) {
        let loginCoordinator = LoginCoordinator()
//        let homeCoordinator = HomeCoordinator()
//        let tabBarController = TabbarViewController()
        var navigationController = UIViewController()
        
        
        navigationController = loginCoordinator.rootViewController
        
        super.init(rootViewController: navigationController)
        addChildCoordinator(loginCoordinator)
    }
    
}
