//
//  LoginCoordinator.swift
//  Tabbar Coordinator
//
//  Created by BRQ on 28/11/18.
//  Copyright © 2018 BRQ. All rights reserved.
//

import UIKit

class LoginCoordinator: BaseCoordinator {
    
    let navigationController: UINavigationController
    let tabCoordinator : TabbarCoordinator!
    
    
    let artistFetcher = FakeArtistFetcher()
    let songFetcher = FakeSongFetcher()
    
    init() {
        
        let appDependencies = AppDependencies(artistFetcher: artistFetcher,
                                              songFetcher: songFetcher)
         tabCoordinator = TabbarCoordinator(dependencies: appDependencies)
        
        let loginViewController = LoginViewController.instantiate()
        
        navigationController = UINavigationController(rootViewController: loginViewController)
        navigationController.navigationBar.isHidden = true
        
        super.init(rootViewController: navigationController)
        loginViewController.delegate = self
    }
}

extension LoginCoordinator: LoginViewControllerDelegate {
    func loginViewController(_ libraryViewController: LoginViewController) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window?.rootViewController = tabCoordinator.rootViewController
        
    }
    
    
}
