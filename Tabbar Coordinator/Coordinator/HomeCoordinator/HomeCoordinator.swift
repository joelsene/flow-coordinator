//
//  HomeCoordinator.swift
//  Tabbar Coordinator
//
//  Created by BRQ on 29/11/18.
//  Copyright © 2018 BRQ. All rights reserved.
//

import UIKit

class HomeCoordinator: BaseCoordinator {
    let navigationController: UINavigationController
    init() {
        let homeViewController = HomeViewController.instantiate()
        navigationController = UINavigationController(rootViewController: homeViewController)
        super.init(rootViewController: navigationController)
        homeViewController.delegate = self
    }
}


extension HomeCoordinator: HomeViewControllerDelegate {
    func pushViewController(_ pushViewController: HomeViewController, text: String) {
        let perfilViewController = ProfileViewController.instantiate()
        perfilViewController.delegate = self
        perfilViewController.receivedText = text
        navigationController.pushViewController(perfilViewController, animated: true)
    }
    
    func presentViewController(_ presentViewController: HomeViewController, text: String) {
        let perfilViewController = ProfileViewController.instantiate()
        perfilViewController.delegate = self
        perfilViewController.receivedText = text
        navigationController.present(perfilViewController, animated: true, completion: nil)
    }
}

extension HomeCoordinator: ProfileViewControllerDelegate {
    func artistsViewController(_ artistsViewController: ProfileViewController) {
            navigationController.dismiss(animated: true, completion: nil)
    }
}
