//
//  MenuCoordinator.swift
//  Tabbar Coordinator
//
//  Created by BRQ on 29/11/18.
//  Copyright © 2018 BRQ. All rights reserved.
//

import UIKit

class MenuCoordinator: BaseCoordinator {
    let artistFetcher = FakeArtistFetcher()
    let songFetcher = FakeSongFetcher()
    
    init() {
        let menuViewController = MenuViewController.instantiate()
        let navigationController = UINavigationController(rootViewController: menuViewController)
        super.init(rootViewController: navigationController)
        menuViewController.delegate = self
        
    }

}

extension MenuCoordinator: MenuViewControllerDelegate {
    func menuViewController(_ menuViewController: MenuViewController) {
        
        let appDependencies = AppDependencies(artistFetcher: artistFetcher,
                                              songFetcher: songFetcher)
        let appCoordinator = AppCoordinator(dependencies: appDependencies)
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window?.rootViewController = appCoordinator.rootViewController
    }
    
    
}

